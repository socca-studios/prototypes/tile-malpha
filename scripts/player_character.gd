extends CharacterBody2D

## Speed in pixels per second
@export_range(0, 1000) var speed := 60

func _physics_process(_delta: float) -> void:
	get_player_input()
	if move_and_slide():
		resolve_collision()


func get_player_input() -> void:
	var vector := Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = vector * speed


func resolve_collision() -> void:
	for collision_id in get_slide_collision_count():
		var collision := get_slide_collision(collision_id)
		var collided_body := collision.get_collider() as MovableObject
		
		if collided_body:
			collided_body.apply_impact(velocity)
